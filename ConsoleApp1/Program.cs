﻿// See https://aka.ms/new-console-template for more information

using ClassLibrary1;
using ClassLibrary2._0;
using ConsoleApp1;
using ConsoleApp1.Models;
using ConsoleApp1.Repository;


//Console.WriteLine("Hello, World!");

#region I/O
//Console.WriteLine($"Enter Product Name: ");
//string pName = Console.ReadLine();
//Console.WriteLine($"Enter Product Price: ");
//float pPrice = float.Parse(Console.ReadLine());
//Console.WriteLine($"Enter Product Description: ");
//string pDesc = Console.ReadLine();

//Console.WriteLine($"Product Details");
//Console.WriteLine($"Product Name: {pName} \nProduct Price: {pPrice} \nProduct Description: {pDesc}");
#endregion

#region Library
//LibProduct prod1 = new LibProduct();
//Console.WriteLine($"Enter Product Name: ");
//prod1.name = Console.ReadLine();
//Console.WriteLine($"Enter Product Price: ");
//prod1.price = float.Parse(Console.ReadLine());
//Console.WriteLine($"Enter Product Description: ");
//prod1.desc = Console.ReadLine();
//Console.WriteLine($"Product Details");
//Console.WriteLine(prod1.details());
#endregion

#region Operator
//Console.WriteLine($"Enter Username: ");
//string uname = Console.ReadLine();
//Console.WriteLine($"Enter Password: ");
//string pass = Console.ReadLine();

//if (uname == "abc" && pass == "123")
//{
//    Console.WriteLine($"Authentication Sucessfull");
//}
//else
//{
//    Console.WriteLine($"Authentication Unsucessfull");
//}

//int num1 = int.Parse(Console.ReadLine());
//if(num1 > 1 || num1 == 1)
//{
//    Console.WriteLine($"Number is greater than or equal to 1");
//}
#endregion

//char a = default;
//Console.WriteLine(a);

#region Old-Libray
Hero sh = new Hero();
sh.name = "Hop Scotch";
sh.age = 20;
sh.power = "Super Strength";
sh.details();

//Villan sv = new Villan();
//sv.name = "Hop Scotch";
//sv.age = 20;
//sv.power = "Super Strength";
//sv.details();
#endregion

#region struct array
//ProductStruct[] products = new ProductStruct[3];
//ProductStruct[][] productsef = new ProductStruct[3][];
//{
//    new ProductStruct(1, "Charging Cabel", "Black mobile charging cabel", 100.0f),
//    new ProductStruct(2, "Charging Adapter", "Black mobile charging adapter", 200.0f),
//    new ProductStruct(3, "Magnets", "Silver magnets", 50.0f),
//};

//Console.WriteLine("Available Products ->");
//foreach (ProductStruct product in products)
//{
//    Console.WriteLine($"ID: {product.id} \tName: {product.name} \tDescription: {product.description} \tPrice: {product.price}");
//}

//Console.WriteLine($"Which product would you like to buy? ->");
//int productChoice = int.Parse(Console.ReadLine());
//Console.WriteLine($"How many would you like to buy? ->");
//int productQuantity = int.Parse(Console.ReadLine());
//Console.WriteLine($"Your Total: {products[productChoice - 1].price * productQuantity}");
#endregion

#region class array
//ProductClass[] productsc = new ProductClass[3];
//ProductClass[][] productsefc = new ProductClass[3][];
//{
//    new ProductClass(1, "Charging Cabel", "Black mobile charging cabel", 100.0f),
//    new ProductClass(2, "Charging Adapter", "Black mobile charging adapter", 200.0f),
//    new ProductClass(3, "Magnets", "Silver magnets", 50.0f),
//};

//Console.WriteLine("Available Products ->");
//foreach (ProductClass product in productsc)
//{
//    Console.WriteLine($"ID: {product.id} \tName: {product.name} \tDescription: {product.description} \tPrice: {product.price}");
//}

//Console.WriteLine($"Which product would you like to buy? ->");
//int productChoicec = int.Parse(Console.ReadLine());
//Console.WriteLine($"How many would you like to buy? ->");
//int productQuantityc = int.Parse(Console.ReadLine());
//Console.WriteLine($"Your Total: {productsc[productChoicec - 1].price * productQuantityc}");
#endregion

#region Login
//Console.WriteLine("Enter Email");
//string email = Console.ReadLine();
//Console.WriteLine("Enter Password");
//string pass = Console.ReadLine();
//UserCredentials uc1 = new UserCredentials(email, pass);
//UserRepo myur = new UserRepo();
//myur.login(uc1);
#endregion

#region goto
//Console.WriteLine("Register and Login 3 accounts");

//int counter = 0;
//Start:
//if(counter > 3)
//{
//    goto End;
//}
//UserRepo ur = new UserRepo();
//Console.WriteLine("\n------Register------");
//Console.WriteLine("Enter Email");
//string uemail = Console.ReadLine();
//Console.WriteLine("Enter Password");
//string upass = Console.ReadLine();
//Console.WriteLine("Enter Name");
//string uname = Console.ReadLine();
//Console.WriteLine("Enter City");
//string ucity = Console.ReadLine();

//User u = new User(uemail, upass, uname, ucity);
//int regStatus = ur.register(u);
//if(regStatus == -1)
//{
//    Console.WriteLine($"Invalid Fields; try again");
//    goto Start;
//} else
//{
//    Console.WriteLine($"Registeration Succuessfull");
//}

//Login:
//Console.WriteLine("------Login------");
//Console.WriteLine("Enter Email");
//string lemail = Console.ReadLine();
//Console.WriteLine("Enter Password");
//string lpass = Console.ReadLine();
//UserCredentials uc = new UserCredentials(lemail, lpass);
//bool loginStatus = ur.login(uc);
//if(!loginStatus)
//{
//    goto Login;
//}

//goto Start;

//End:
//Console.WriteLine("\nThank You; Visit Again");
#endregion

#region Protected Internal
Protect ptd = new Protect();
ptd.Run();

#endregion