﻿using System;

namespace ConsoleApp1
{
    internal struct ProductStruct
    {
        public int id;
        public string name;
        public string description;
        public float price;

        public ProductStruct(int id, string name, string description, float price)
        {
            this.id = id;
            this.name = name;
            this.description = description;
            this.price = price;
        }

    }

    internal class ProductClass
    {
        public int id;
        public string name;
        public string description;
        public float price;

        public ProductClass(int id, string name, string description, float price)
        {
            this.id = id;
            this.name = name;
            this.description = description;
            this.price = price;
        }

    }
}
