﻿using System;

namespace ConsoleApp1
{
    public class MyClass1
    {
        protected string name = "Hello World";
        protected internal int number = 11;
    }

    public class MyClass2 : MyClass1
    {
        public void MyFunc()
        {
            Console.WriteLine(name);
            Console.WriteLine(number);
        }
    }

    class MyClass3
    {
        public void MyFunc()
        {
            MyClass1 mc1 = new MyClass1();
            //Console.WriteLine(mc1.name); // gives error
            Console.WriteLine(mc1.number);
        }

    }

    public class Protect
    {
        public void Run()
        {
            MyClass2 mc2 = new MyClass2();
            //Console.WriteLine(mc2.name); // gives error
            Console.WriteLine(mc2.number);
            mc2.MyFunc();

            MyClass3 mc3 = new MyClass3();
            mc3.MyFunc();
        }
    }
}
