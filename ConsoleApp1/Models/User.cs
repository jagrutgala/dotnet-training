﻿using System;

namespace ConsoleApp1.Models
{
    internal class User
    {
        public string email;
        public string pass;
        public string name;
        public string city;

        public User(string email, string pass, string name, string city)
        {
            this.email = email;
            this.pass = pass;
            this.name = name;
            this.city = city;
        }

        public UserCredentials GetUserCredentials()
        {
            return new UserCredentials(email, pass);
        }
    }
}
