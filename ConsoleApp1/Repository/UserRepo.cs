﻿using ConsoleApp1.Models;
using System;

namespace ConsoleApp1.Repository
{
    internal class UserRepo
    {
        public int lastIndx;
        public User[] userList;

        public UserRepo()
        {
            userList = new User[10];
            lastIndx = 0;

            //register(new User("ab@mail.com", ))

        }

        public void printAllUsers()
        {
            for (int i = 0; i < userList.Length; i++)
            {
                User user = userList[i];
                Console.WriteLine($"idx:{i + 1} name: {user.name} \temail:{user.email} \tcity{user.city}");
            }
            Console.WriteLine();
        }

        public void login(string username, string password)
        {
            if (username == null || password == null || username == "" || password == " ")
            {
                Console.WriteLine("Enter all credentials to continue");
            }
            else if (username == "abcd" && password == "1234")
            {
                Console.WriteLine("Login Successful");

            }
            else
            {
                Console.WriteLine("Login Unsuccessful");
            }
        }

        public void login(UserCredentials creds)
        {
            if (creds.email == null || creds.pass == null || creds.email == "" || creds.email == " ")
            {
                Console.WriteLine("Enter all credentials to continue");
            }
            else if (creds.email == "abcd" && creds.pass == "1234")
            {
                Console.WriteLine("Login Successful");
            }
            else
            {
                Console.WriteLine("Login Unsuccessful");
            }

        }

        public bool loginr(UserCredentials creds)
        {
            //check if any input is null
            if (creds.email == null || creds.pass == null || creds.email == "" || creds.pass == "")
            {
                Console.WriteLine("Enter all credentials to continue");
            }

            User fuser = null;
            for (int i = 0; i < userList.Length; i++)
            { // search user
                if (userList[i].email == creds.email)
                {
                    fuser = userList[i];
                    break;
                }
            }

            if (fuser == null) // if user not found
            {
                Console.WriteLine("Invalid Credentials try again");
                return false;
            }
            if (creds.pass == fuser.pass) // authenticate credentails
            {
                Console.WriteLine("Login Successful");
                return true;

            }
            Console.WriteLine("Login Unsuccessful");
            return false;
        }

        public int register(User u)
        {
            if (u.email == null || u.email == null || u.pass == null || u.city == null ||
                u.email == "" || u.email == "" || u.pass == "" || u.city == "")
            {
                Console.WriteLine("Enter all credentials to continue");
                return -1;
            }
            userList[lastIndx] = u;
            lastIndx = ++lastIndx % userList.Length;
            return lastIndx;
        }

    }
}
