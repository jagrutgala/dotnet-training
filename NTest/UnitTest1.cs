using Assignments;

namespace NTest
{
    public class Tests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void testToFahrenheit()
        {
            var temp = 32.9;
            var expectedOut = 91.22;
            var actualOut = TempConvert.toFahrenheit(temp);
            Assert.AreEqual(expectedOut, actualOut);
        }
        [Test]
        public void testToCelcius()
        {
            var temp = 91.22;
            var expectedOut = 32.9;
            var actualOut = TempConvert.toCelsius(temp);
            Assert.AreEqual(expectedOut, actualOut);
        }
    }
}