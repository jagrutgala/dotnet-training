﻿namespace ClassLibrary1
{
    public class LibProduct
    {
        public string name;
        public float price;
        public string desc;

        public string details()
        {
            return $"Product Name: {name} Product Price: {price} Product Description: {desc}";
        }
    }
}