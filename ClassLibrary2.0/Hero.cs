﻿using System;

namespace ClassLibrary2._0
{
    public class Hero
    {
        public string name;
        public int age;
        public string power;

        public string details()
        {
            return $"Name: {name} Age: {age} Super Power: {power}";
        }
    }
}
