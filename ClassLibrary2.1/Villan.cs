﻿using System;

namespace ClassLibrary2._1
{
    public class Villan
    {
        public string name;
        public int age;
        public string power;

        public string details()
        {
            return $"Name: {name} Age: {age} Super Power: {power}";
        }

    }
}
