﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterfaceAndInheritance.Models
{
    internal class Vehicle // Base Class
    {
        public string numPlate;
        public int fuelCap;
        public double millage;

        public Vehicle(string numPlate, int fuelCap, double millage)
        {
            this.numPlate = numPlate;
            this.fuelCap = fuelCap;
            this.millage = millage;
        }

        public void Accelerate()
        {
            Console.WriteLine("Brrrrr......");
        }
        public void Brake()
        {
            Console.WriteLine("Shknnnnnnn.........");
        }

    }


    internal class Car : Vehicle // Hierarchical Inheritance
    {
        public bool aircond;
        public Car(string numplate, int fuelCap, double millage, bool aircond): base(numplate, fuelCap, millage)
        {
            this.aircond = aircond;
        }

        public void Accelerate()
        {
            Console.WriteLine("Vhrummmm.........");
        }
    }

    internal class Truck : Vehicle // Hierarchical Inheritance
    {
        int storage;
        public Truck(string numplate, int fuelCap, double millage, int storage) : base(numplate, fuelCap, millage)
        {
            this.storage = storage;
        }
    }

    internal class Bike : Vehicle // Hierarchical Inheritance
    {
        int gear;
        public Bike(string numplate, int fuelCap, double millage, int gear) : base(numplate, fuelCap, millage)
        {
            this.gear = gear;
        }
    }


    internal class Bullet : Bike // multi-level inheritance
    {
        int sliencerPower;

        public Bullet(string numplate, int fuelCap, double millage, int gear, int silencerPower) : base(numplate, fuelCap, millage, gear)
        {
            this.sliencerPower = silencerPower;

        }
    }
}
