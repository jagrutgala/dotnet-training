﻿using System;
using InterfaceAndInheritance.Constatnts;
using InterfaceAndInheritance.Models;

namespace InterfaceAndInheritance.Repository
{
    internal class ProdcutRepo : IOrder, ICart
    {
        List<Product> products;

        public ProdcutRepo()
        {
            this.products = new List<Product>();
        }

        public void AddToCart(Product p)
        {
            products.Add(p);
            Console.WriteLine($"Product added successfully");
        }

        public List<Product> BuyOrder()
        {
            return products;
        }

        public void CancelOrder()
        {
            products.Clear();
        }

        public void PrintOrderInvoice(Invoice.InvoiceOptions iv)
        {
            double amount = 0;
            foreach (Product item in products)
            {
                amount += item.price;
            }
            Console.WriteLine($"Total Cost: {amount}");
            switch (iv)
            {
                case Invoice.InvoiceOptions.Mail:
                    Console.WriteLine("Mailing Invoice");
                    break;
                case Invoice.InvoiceOptions.PDF:
                    Console.WriteLine("Downloading Invoice");
                    break;
                case Invoice.InvoiceOptions.SMS:
                    Console.WriteLine("Sending Invoice");
                    break;
                default:
                    Console.WriteLine("Invalid Invoice");
                    break;
            }
        }

        public void PrintOrderItems()
        {
            for (int i = 0; i < products.Count; i++)
            {
                Console.WriteLine( $"{i} - {products[i]}");
            }
            Console.WriteLine("\n");
        }

        public void RemoveFromCart(Product p)
        {
            products.Remove(p);
            Console.WriteLine($"Product removed successfully");
        }

        public void RemoveFromCartByName(string name)
        {
            Product p = products.Find(item => item.name == name);
            Console.WriteLine($"Found : {p}");
            this.RemoveFromCart(p);
        }
    }
}
