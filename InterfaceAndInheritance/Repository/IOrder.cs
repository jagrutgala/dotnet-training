﻿using System;
using InterfaceAndInheritance.Constatnts;
using InterfaceAndInheritance.Models;

namespace InterfaceAndInheritance.Repository
{

    internal interface IOrder
    {
        List<Product> BuyOrder();
        void CancelOrder();

        void PrintOrderItems();

        void PrintOrderInvoice(Invoice.InvoiceOptions iv);
    }
}
