﻿using System;
using InterfaceAndInheritance.Models;

namespace InterfaceAndInheritance.Repository
{
    internal interface ICart
    {
        void AddToCart(Product p);
        void RemoveFromCart(Product p);
    }
}
