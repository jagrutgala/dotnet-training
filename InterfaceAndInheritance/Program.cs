﻿// See https://aka.ms/new-console-template for more information
using InterfaceAndInheritance.Repository;
using InterfaceAndInheritance.Models;
using InterfaceAndInheritance.Constatnts;

Console.WriteLine("Hello, World!");

#region Interface Example
//ProdcutRepo productRepo = new ProdcutRepo();

//Product p1 = new Product("Mobile", 3000);
//productRepo.AddToCart(p1);
//Product p2 = new Product("Television", 2000);
//productRepo.AddToCart(p2);
//Product p3 = new Product("Laptop", 4000);
//productRepo.AddToCart(p3);
//Product p4 = new Product("Keyboard", 2500);
//productRepo.AddToCart(p4);
//Product p5 = new Product("Mouse", 1000);
//productRepo.AddToCart(p5);
//productRepo.PrintOrderItems();

//productRepo.RemoveFromCart(p2);
//productRepo.PrintOrderItems();

//Console.WriteLine("Enter Product name that you want to remove:");
//string pname = Console.ReadLine().Trim();
//productRepo.RemoveFromCartByName(pname);
//productRepo.PrintOrderItems();

//List<Product> myOrder = productRepo.BuyOrder();
//for (int i = 0; i < myOrder.Count; i++)
//{
//    Console.WriteLine($"{i} - {myOrder[i]}");
//}

#endregion



#region Inheritance Example
//Vehicle vh = new Vehicle("MH123", 100, 20);

//Car c1 = new Car("MH34", 23, 2345, true);
//c1.Accelerate();

//Truck t1 = new Truck("sd2314", 234, 26, 279);

//Bike b1 = new Bike("sk233", 23, 457, 4);

//Bullet b2 = new Bullet("me234", 23, 345, 157, 6839);

#endregion


#region Enum
EnumsDemo.Run();

ProdcutRepo productRepo = new ProdcutRepo();

Product p1 = new Product("Mobile", 3000);
productRepo.AddToCart(p1);
Product p2 = new Product("Television", 2000);
productRepo.AddToCart(p2);
Product p3 = new Product("Laptop", 4000);
productRepo.AddToCart(p3);
Product p4 = new Product("Keyboard", 2500);
productRepo.AddToCart(p4);
Product p5 = new Product("Mouse", 1000);
productRepo.AddToCart(p5);
productRepo.PrintOrderItems();

Console.WriteLine("Invoice Options Available");
Invoice.InvoiceOptions iv = (Invoice.InvoiceOptions)int.Parse(Console.ReadLine());
productRepo.PrintOrderInvoice(iv);

#endregion