﻿using System;

namespace InterfaceAndInheritance.Constatnts
{
    internal class EnumsDemo
    {
        enum Number
        {
            Zero, One, Two, Three
        }

        public static void Run()
        {
            Console.WriteLine($"{(int)Number.One}{Number.One} {(int)Number.Two}{Number.Two} {(int)Number.Three}{Number.Three}");
        }

    }
}
