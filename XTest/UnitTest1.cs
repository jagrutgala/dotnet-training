using Assignments;
namespace XTest
{
    public class UnitTest1
    {
        [Fact]
        public void testToFahrenheit()
        {
            var temp = 32.9;
            var expectedOut = 91.22;
            var actualOut = TempConvert.toFahrenheit(temp);
            Assert.Equal(expectedOut, actualOut);
        }
        [Fact]
        public void testToCelcius()
        {
            var temp = 91.22;
            var expectedOut = 32.9;
            var actualOut = TempConvert.toCelsius(temp);
            Assert.Equal(expectedOut, actualOut);
        }
    }
}