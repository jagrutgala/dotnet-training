﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignments.Model
{
    internal class Product
    {
        public int id;
        public string name;
        public string category;
        public double Price { get; set; }

        public Product(int id, string name, float price, string category) // constructor
        {
            this.id = id;
            this.name = name;
            this.category = category;
            this.Price = price;
        }


        public string getDetails() // string of product details
        {
            return $"Id:{id} Name:{name} Category:{category} Price:{Price}";
        }

        public override string ToString() // string of product details
        {
            return $"Id:{id} Name:{name} Category:{category} Price:{Price}";
        }
    }
}
