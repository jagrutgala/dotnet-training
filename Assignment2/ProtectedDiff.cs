﻿using System;
using ConsoleApp1;

namespace Assignments
{

    public class MyClass4 : MyClass1
    {
        public void MyFunc()
        {
            Console.WriteLine(name);
            Console.WriteLine(number);
        }
    }

    public class MyClass5
    {
        public void MyFunc()
        {
            MyClass1 mc1 = new MyClass1();
            //Console.WriteLine(mc1.name); // gives error
            //Console.WriteLine(mc1.number); // gives error
        }
    }

    internal class ProtectedDiff
    {
        public void Run()
        {
            MyClass4 mc4 = new MyClass4();
            //Console.WriteLine(mc4.name); // gives error
            //Console.WriteLine(mc4.number); // gives error
            mc4.MyFunc();

            MyClass5 mc5 = new MyClass5();
            //Console.WriteLine(mc4.name); // gives error
            //Console.WriteLine(mc4.number); // gives error
            mc5.MyFunc();
        }

    }

}
