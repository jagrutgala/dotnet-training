﻿// See https://aka.ms/new-console-template for more information
using Assignments.Model;
using Assignments.Repository;
using Assignments;

#region Methods & Out
//Product p1 = new Product(1, "TV", 1000.0f, "Mobile");
//ProductRepository pr = new ProductRepository();

//int id;
//string name;
//float price;

//pr.getProductDetails(p1, out id, out name, out price);
//Console.WriteLine($"Id: {id}, Name:{name}, Price:{price}");
#endregion


#region Class Array & Property
//ProductRepository prepo = new ProductRepository();
//Product[] productArr = prepo.getAllProdducts();
//foreach (Product item in productArr)
//{
//    Console.WriteLine($"{item.getDetails()}");
//}
//Console.WriteLine("\n");

//Product[] filteredProduct = prepo.getProductByCat("Mobile");
//foreach (Product item in filteredProduct)
//{
//    Console.WriteLine(item.getDetails());
//}

#endregion


#region Protected Internal
//ProtectedDiff ptdf = new ProtectedDiff();
//ptdf.Run();
#endregion


#region Class Array Operations
ProductRepository prepo = new ProductRepository();
prepo.printAllProducts();
Console.WriteLine("\n");
Console.WriteLine("Which product do you want to delte? (Plz enter id.)");
int pid = int.Parse(Console.ReadLine().Trim());
prepo.DeleteProduct(pid);

Console.WriteLine("\n");
prepo.printAllProducts();
Console.WriteLine("Which product do you want to update? (Plz enter id.)");
int pupid = int.Parse(Console.ReadLine().Trim());
Console.WriteLine("Enter new Name? (enter if want to keep same)");
string pname = Console.ReadLine().Trim();
Console.WriteLine("Enter new Category? (enter if want to keep same)");
string pcat = Console.ReadLine().Trim();
Console.WriteLine("Enter new Price? (enter if want to keep same)");
double pprice = double.Parse(Console.ReadLine().Trim());
prepo.UpdateProduct(pupid, pname, pcat, pprice);

Console.WriteLine("\n");
prepo.printAllProducts();

#endregion