﻿using Assignments.Model;
using System;

namespace Assignments.Repository
{
    internal class ProductRepository
    {
        Product[] productList = new Product[4];

        public ProductRepository()
        {
            productList[0] = new Product(1, "AZ", 100f, "Mobile");
            productList[1] = new Product(2, "AX", 500f, "Furniture");
            productList[2] = new Product(3, "AC", 150f, "Mobile");
            productList[3] = new Product(4, "AV", 200f, "Decoration");
        }
        public void getProductDetails(Product p, out int id, out string name, out double price)
        {
            id = p.id;
            name = p.name;
            price = p.Price;

        }

        public Product[] getAllProdducts()
        {
            return productList;
        }

        public Product[] getProductByCat(string cat)
        {
            return Array.FindAll(productList, item => item.category == cat);

        }
        public void printAllProducts()
        {
            for (int i = 0; i < productList.Length; i++)
            {
                Product item = productList[i];
                Console.WriteLine($"{item.getDetails()}");
            }
        }
        public void DeleteProduct(int id)
        {
            productList = Array.FindAll(productList, item => item.id != id);
        }

        public void UpdateProduct(int id, string name, string cat, double price)
        {
            int indx = Array.FindIndex(productList, item => item.id == id);
            if (indx > -1)
            {

                Console.WriteLine($"Name:{name}");
                if (name != null && name != "")
                {
                    productList[indx].name = name;
                }
                if (cat != null && cat != "")
                {
                    productList[indx].category = cat;
                }
                if (price > 0)
                {
                    productList[indx].Price = price;
                }
            }
            else {
                Console.WriteLine("Such product doesnot exist");
            }
        }
    }
}
