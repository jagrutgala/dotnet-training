﻿using System;

namespace Assignments
{
    public class TempConvert
    {

        public static double toFahrenheit(double cTemp)
        {
            return (cTemp * 1.8) + 32;
        }
        public static double toCelsius(double fTemp)
        {
            return (fTemp - 32) / 1.8;
        }

    }
}
